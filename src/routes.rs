use axum::{
    middleware,
    routing::{delete, get, patch, post, put},
    Router,
};
use sea_orm::DatabaseConnection;
mod atomic_update;
mod create_task;
mod create_user;
mod delete_task;
mod get_all_task;
mod get_one_task;
mod login;
mod logout;
mod partial_update;

use atomic_update::atomic_update;
use create_task::create_task;
use create_user::signup;
use delete_task::delete_task;
use get_all_task::get_all_task;
use get_one_task::get_one_task;
use partial_update::partial_update;

use login::login;
use logout::logout;

use crate::util::guard_middleware::guard;

pub fn app(database: DatabaseConnection) -> Router {
    Router::new()
        .route("/", get(|| async { format!("hello World!!") }))
        .route("/task", post(create_task))
        .route("/task/:task_id", get(get_one_task))
        .route("/task", get(get_all_task))
        .route("/task/:task_id", put(atomic_update))
        .route("/task/:task_id", patch(partial_update))
        .route("/task/:task_id", delete(delete_task))
        .route("/logout", get(logout))
        .route_layer(middleware::from_fn_with_state(database.clone(), guard))
        
        .route("/signup", post(signup))
        .route("/login", post(login))
        //.layer(State(database))
        .with_state(database)
}
