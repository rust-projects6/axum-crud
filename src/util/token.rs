use chrono::{Duration, Utc};
use dotenvy_macro::dotenv;
use jsonwebtoken::{DecodingKey, EncodingKey, Header, Validation};
use serde::{Deserialize, Serialize};

use super::app_err::AppErr;

#[derive(Debug, Serialize, Deserialize)]
struct Claim {
    ita: usize,
    exp: usize,
}

impl Default for Claim {
    fn default() -> Self {
        let now = Utc::now();
        let ita = now.timestamp() as usize;
        let exp = (now + Duration::days(30)).timestamp() as usize;

        Self { ita, exp }
    }
}

pub fn create<'a>() -> Result<String, AppErr<'a>> {
    let secret = dotenv!("JWT_SECRET"); //"JWT_SECRET";

    let token = jsonwebtoken::encode(
        &Header::default(),
        &Claim::default(),
        &EncodingKey::from_secret(secret.as_ref()),
    )
    .map_err(|_| AppErr::Internal)?;

    Ok(token)
}

pub fn is_valid<'a>(token: &str) -> Result<bool, AppErr<'a>> {
    let secret = dotenv!("JWT_SECRET");

    jsonwebtoken::decode::<Claim>(
        token,
        &DecodingKey::from_secret(secret.as_ref()),
        &Validation::default(),
    )
    .map_err(|error| match error.kind() {
        jsonwebtoken::errors::ErrorKind::InvalidToken => AppErr::Validation,
        jsonwebtoken::errors::ErrorKind::InvalidSignature => AppErr::Validation,

        jsonwebtoken::errors::ErrorKind::ExpiredSignature => AppErr::UnAuthorize,
        _ => AppErr::Internal,
    })?;

    Ok(true)
}

#[cfg(test)]
mod tests {
    use super::*;
    #[tokio::test]
    async fn token_validation() {
        let token = create().map_err(|err|err).unwrap();

        assert_eq!(is_valid(&token), Ok(true));

    }
}
