
use bcrypt::{hash, verify, DEFAULT_COST};

use super::app_err::AppErr;
pub trait HashedPass {
    fn get_pass<'a>(&'a self) -> &'a str;
    fn get_hashed_pass<'a>(&'a self) -> Result<String, AppErr<'a>> {
        hash(self.get_pass(), DEFAULT_COST).map_err(|_err| AppErr::Internal)
    }
    fn is_verify<'a>(&'a self, hashed: &str) -> Result<bool, AppErr<'a>> {
        verify(self.get_pass(), hashed).map_err(|_err| AppErr::Internal)
    }
}
