use axum::headers::authorization::Bearer;
use axum::headers::Authorization;
use axum::{extract::State, http::Request, middleware::Next, response::Response, TypedHeader};
use sea_orm::{ColumnTrait, DatabaseConnection, EntityTrait, QueryFilter};

use crate::entity::prelude::Users;
use crate::entity::users;

use super::app_err::AppErr;
use super::token::is_valid;

pub async fn guard<'a, T>(
    State(db): State<DatabaseConnection>,
    TypedHeader(token): TypedHeader<Authorization<Bearer>>,
    mut request: Request<T>,
    next: Next<T>,
) -> Result<Response, AppErr<'a>> {
    let token = token.token();

    let user = Users::find()
        .filter(users::Column::Token.eq(Some(token)))
        .one(&db)
        .await
        .map_err(|_| AppErr::DBConn)?
        .ok_or(AppErr::NotFound)?;

    match is_valid(token)? {
        true => {
            request.extensions_mut().insert(user);
            Ok(next.run(request).await)
        }
        false => Err(AppErr::UnAuthorize),
    }
}
