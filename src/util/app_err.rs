use axum::{http::StatusCode, response::IntoResponse};

#[derive(Debug, PartialEq)]
pub enum AppErr<'a> {
    NotFound,
    DBConn,
    IOErr,
    Validation,
    UnAuthorize,
    Internal,
    Other(StatusCode, &'a str),
}

// impl<'a> Deref for AppErr<'a> {
//     type Target = AppErr<'static>;

//     fn deref(&self) -> &Self::Target {
//         self
//     }
// }

impl<'a> IntoResponse for AppErr<'a> {
    fn into_response(self) -> axum::response::Response {
        let res = match self {
            AppErr::NotFound => (StatusCode::NOT_FOUND, "The Records Not Found"),
            AppErr::DBConn => (StatusCode::INTERNAL_SERVER_ERROR, "DataBase not connected"),
            AppErr::Internal => (StatusCode::INTERNAL_SERVER_ERROR, "Something Went Wrong!!"),
            AppErr::IOErr => (StatusCode::INTERNAL_SERVER_ERROR, "IO Error"),
            AppErr::Validation => (StatusCode::BAD_REQUEST, "Validation Error"),
            AppErr::UnAuthorize => (StatusCode::UNAUTHORIZED, "Authentication Fail"),
            AppErr::Other(code, msg) => {
                let a = msg.to_owned().into_boxed_str();
                
                let msg:&'static mut str = Box::leak(a);

                (code, &*msg)
            }
        };
        res.into_response()
    }
}
