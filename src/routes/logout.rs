use axum::{extract::State, Extension};
use sea_orm::{ActiveModelTrait, DatabaseConnection, IntoActiveModel, Set};

use crate::{entity::users::Model, util::app_err::AppErr};

pub async fn logout<'a>(
    State(db): State<DatabaseConnection>,
    Extension(user): Extension<Model>,
) -> Result<(), AppErr<'a>> {
    let user = {
        let mut user = user.into_active_model();
        user.token = Set(None);
        user
    }
    .update(&db)
    .await
    .map_err(|_| AppErr::DBConn)?;

    Ok(())
}
