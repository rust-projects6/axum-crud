use crate::{entity::{prelude::Tasks, tasks}, util::app_err::AppErr};
use axum::{extract::{Query, State}, Json};
use sea_orm::{ColumnTrait, Condition, DatabaseConnection, EntityTrait, QueryFilter};
use serde::Deserialize;

use crate::types::response_task::ResponseTask;

#[derive(Deserialize, Clone)]
pub struct TaskQuery {
    pub priority: Option<String>,
}

pub async fn get_all_task<'a>(
    State(database): State<DatabaseConnection>,
    Query(query_param): Query<TaskQuery>,
) -> Result<Json<Vec<ResponseTask>>, AppErr<'a>> {
    // let priority_filter = match query_param.priority {
    //     Some(value) if value.is_empty() => Condition::all().add(tasks::Column::Priority.is_null()),
    //     Some(value) => Condition::all().add(tasks::Column::Priority.eq(value)),
    //     None => Condition::all(),
    // };

    let tasks = Tasks::find()
        .filter(get_filter(&query_param.priority))
        .all(&database)
        .await
        .map_err(|_error| AppErr::DBConn)
        .unwrap()
        .into_iter()
        .map(|task| ResponseTask {
            id: task.id,
            title: task.title,
            description: task.description,
            priority: task.priority,
        })
        .collect();

    Ok(Json(tasks))
}

fn get_filter(filter: &Option<String>) -> Condition {
    match filter {
        Some(value) if value.is_empty() => Condition::all().add(tasks::Column::Priority.is_null()),
        Some(value) => Condition::all().add(tasks::Column::Priority.eq(value)),
        None => Condition::all(),
    }

    // filter
    //     .as_ref()
    //     .and_then(|v| {
    //         if v.is_empty() {
    //             return Some(Condition::all().add(tasks::Column::Priority.is_null()));
    //         }
    //         return Some(Condition::all().add(tasks::Column::Priority.eq(v)));
    //     })
    //     .or_else(|| Some(Condition::all()))
    //     .unwrap()
}
