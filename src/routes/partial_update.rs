use crate::{
    entity::{prelude::Tasks, tasks},
    types::partial_request_task::PartialRequestTask,
    util::app_err::AppErr,
};
use axum::{
    extract::{Path, State},
    Json,
};
use sea_orm::{ColumnTrait, DatabaseConnection, EntityTrait, IntoActiveModel, QueryFilter, Set};

pub async fn partial_update<'a>(
    State(db): State<DatabaseConnection>,
    Path(task_id): Path<i32>,
    Json(request_task): Json<PartialRequestTask>,
) -> Result<(), AppErr<'a>> {
    let mut db_task = Tasks::find_by_id(task_id)
        .one(&db)
        .await
        .map_err(|_| AppErr::DBConn)
        .unwrap()
        .ok_or(AppErr::NotFound)
        .unwrap()
        .into_active_model();

    if let Some(priority) = request_task.priority {
        db_task.priority = Set(priority);
    }

    if let Some(title) = request_task.title {
        db_task.title = Set(title);
    }

    if let Some(description) = request_task.description {
        db_task.description = Set(description);
    }

    if let Some(completed_at) = request_task.completed_at {
        db_task.completed_at = Set(completed_at);
    }

    if let Some(deleted_at) = request_task.deleted_at {
        db_task.deleted_at = Set(deleted_at);
    }

    if let Some(is_default) = request_task.is_default {
        db_task.is_default = Set(is_default.unwrap_or_default());
    }

    Tasks::update(db_task)
        .filter(tasks::Column::Id.eq(task_id))
        .exec(&db)
        .await
        .map_err(|_| AppErr::DBConn)?;

    Ok(())
}
