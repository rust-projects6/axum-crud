use axum::{
    extract::State,
    http::StatusCode,
    response::{IntoResponse, Response},
    Json,
};
use sea_orm::{ActiveModelTrait, DatabaseConnection, Set};

use crate::{
    entity::users,
    types::{user::User, user_response::UserResponse},
    util::{app_err::AppErr, hashed_pass::HashedPass, token},
};

pub async fn signup<'a>(
    State(db): State<DatabaseConnection>,
    Json(user_req): Json<User>,
) -> Result<Response, AppErr<'a>> {
    let token = token::create()?;

    let user = users::ActiveModel {
        password: Set(user_req.get_hashed_pass().unwrap().to_owned()),
        user_name: Set(user_req.user_name.to_owned()),
        token: Set(Some(token)),
        ..Default::default()
    }
    .save(&db)
    .await
    .map_err(|err| match err {
        sea_orm::DbErr::RecordNotInserted => {
            AppErr::Other(StatusCode::BAD_REQUEST, "User already exsist")
        }
        sea_orm::DbErr::RecordNotUpdated => {
            AppErr::Other(StatusCode::BAD_REQUEST, "User already exsist")
        }
        _ => AppErr::DBConn,
    })?;

    let res = UserResponse {
        id: &user.id.unwrap(),
        user_name: &user.user_name.unwrap(),
        token: &user.token.unwrap().unwrap(),
    };

    Ok((StatusCode::CREATED, Json(res)).into_response())
}
