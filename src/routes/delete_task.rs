use crate::{entity::{prelude::Tasks, tasks}, util::app_err::AppErr};
use axum::{
    extract::{Path, Query, State},
};
use sea_orm::{ColumnTrait, DatabaseConnection, EntityTrait, IntoActiveModel, QueryFilter, Set};
use serde::Deserialize;

#[derive(Debug, Deserialize)]
pub struct QueryParams {
    pub soft: Option<bool>,
}

pub async fn delete_task<'a>(
    State(db): State<DatabaseConnection>,
    Path(task_id): Path<i32>,
    Query(query_params): Query<QueryParams>,
) -> Result<(), AppErr<'a>> {
    if query_params.soft.unwrap() {
        let mut task = Tasks::find_by_id(task_id)
            .one(&db)
            .await
            .map_err(|_| AppErr::DBConn)
            .unwrap()
            .ok_or_else(|| AppErr::NotFound)?
            .into_active_model();

        task.deleted_at = Set(Some(chrono::Utc::now().naive_utc().into()));

        Tasks::update(task)
            .filter(tasks::Column::Id.eq(task_id))
            .exec(&db)
            .await
            .map_err(|_err| AppErr::DBConn)?;
    } else {
        Tasks::delete_by_id(task_id)
            .exec(&db)
            .await
            .map_err(|_error| AppErr::DBConn)?;
    }
    Ok(())
}
