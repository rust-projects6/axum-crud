use axum::http::StatusCode;
use axum::response::{IntoResponse, Response};
use axum::{extract::State, Json};
use sea_orm::{ActiveModelTrait, ColumnTrait, IntoActiveModel, Set};
use sea_orm::{DatabaseConnection, EntityTrait, QueryFilter};

use crate::util::token;
use crate::{
    entity::{prelude::Users, users},
    types::{user::User, user_response::UserResponse},
    util::{app_err::AppErr, hashed_pass::HashedPass},
};

pub async fn login<'a>(
    State(db): State<DatabaseConnection>,
    Json(req): Json<User>,
) -> Result<Response, AppErr<'a>> {
    let mut user = Users::find()
        .filter(users::Column::UserName.eq(&req.user_name))
        .one(&db)
        .await
        .map_err(|_| AppErr::DBConn)?
        .ok_or(AppErr::NotFound)?
        .into_active_model();

    match req.is_verify(&user.password.clone().unwrap().to_owned()).unwrap() {
        true => {
            let token = token::create()?;
            let user = {
                user.token = Set(Some(token.clone()));
                user
            }
            .save(&db)
            .await
            .map_err(|_| AppErr::DBConn)?;

            let res = UserResponse {
                user_name: &user.user_name.unwrap(),
                id: &user.id.unwrap(),
                token: &token,
            };
            Ok((StatusCode::OK, Json(res)).into_response())
        }
        false => Err(AppErr::Validation),
    }
}
