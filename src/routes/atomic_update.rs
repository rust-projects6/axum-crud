use axum::{extract::{Path,State}, Json};
use sea_orm::{ColumnTrait, DatabaseConnection, EntityTrait, QueryFilter, Set};

use crate::{entity::prelude::Tasks, util::app_err::AppErr};
use crate::entity::tasks;
use crate::types::request_task::RequestTask;


pub async fn atomic_update<'a>(
    Path(task_id): Path<i32>,
    State(db): State<DatabaseConnection>,
    Json(req_task): Json<RequestTask>,
) -> Result<(), AppErr<'a>> {

    let model = tasks::ActiveModel {
        id: Set(task_id),
        title: Set(req_task.title),
        priority: Set(req_task.priority),
        completed_at: Set(req_task.completed_at),
        description: Set(req_task.description),
        deleted_at: Set(req_task.deleted_at),
        user_id: Set(req_task.user_id),
        is_default: Set(req_task.is_default.unwrap_or_default()),
        ..Default::default()
    };

    Tasks::update(model)
        .filter(tasks::Column::Id.eq(task_id))
        .exec(&db)
        .await
        .map_err(|_| AppErr::DBConn)?;
    Ok(())
}
