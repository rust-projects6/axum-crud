use axum::{extract::State, Json, Extension};
use sea_orm::{ActiveModelTrait, DatabaseConnection, Set};

use crate::{entity::{tasks::{self}, users::{Model}}, types::request_task::RequestTask, util::app_err::AppErr};

pub async fn create_task<'a>(
    State(database): State<DatabaseConnection>,
    Extension(user): Extension<Model>,
    Json(req): Json<RequestTask>,
) -> Result<(), AppErr<'a>> {
    let new_task = tasks::ActiveModel {
        title: Set(req.title.to_owned()),
        priority: Set(req.priority.to_owned()),
        description: Set(req.description.to_owned()),
        user_id: Set(Some(user.id)),

        ..Default::default()
    };

    new_task.save(&database).await.map_err(|_err| AppErr::DBConn)?;
    Ok(())
}
