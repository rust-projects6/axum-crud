use crate::{entity::prelude::Tasks, types::response_task::ResponseTask, util::app_err::AppErr};
use axum::{extract::{Path, State}, Json};
use sea_orm::{DatabaseConnection, EntityTrait};

// pub async fn get_one_task(
//     State(db): State<DatabaseConnection>,
//     Path(task_id): Path<i32>,
// ) -> Result<Json<ResponseTask>, StatusCode> {
//     let res = Tasks::find_by_id(task_id).one(&db).await.unwrap();

//     if let Some(task) = res {
//         Ok(Json(ResponseTask {
//             id: task.id,
//             title: task.title,
//             description: task.description,
//             priority: task.priority,
//         }))
//     } else {
//         Err(StatusCode::NOT_FOUND)
//     }
// }

pub async fn get_one_task<'a>(
    State(db): State<DatabaseConnection>,
    Path(task_id): Path<i32>,
) -> Result<Json<ResponseTask>, AppErr<'a>> {
    let a: Result<Json<ResponseTask>, AppErr> = Tasks::find_by_id(task_id)
        .one(&db)
        .await
        .or_else(|_error| Err(AppErr::DBConn))
        .unwrap()
        .ok_or(AppErr::NotFound)
        .map(|task| {
            Json(ResponseTask {
                id: task.id,
                title: task.title,
                description: task.description,
                priority: task.priority,
            })
        });

    a
}
