use std::net::SocketAddr;
use sea_orm::Database;

mod routes;
mod entity;
mod types;
mod util;

use routes::app;


pub async fn run(uri: &str) -> () {
    let database = Database::connect(uri).await.unwrap();

    dbg!(&database);
    // dbg!(Claim::default());

    let addr = SocketAddr::from(([127, 0, 0, 1], 3000));

    axum::Server::bind(&addr)
        .serve(app(database).into_make_service())
        .await
        .unwrap()
}
