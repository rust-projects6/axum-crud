use sea_orm::entity::prelude::DateTime;
use serde::Deserialize;

#[derive(Debug, Deserialize)]
pub struct PartialRequestTask {
    pub title: Option<String>,
    #[serde(default, with = "::serde_with::rust::double_option")]
    pub priority: Option<Option<String>>,
    #[serde(default, with = "::serde_with::rust::double_option")]
    pub completed_at: Option<Option<DateTime>>,
    #[serde(default, with = "::serde_with::rust::double_option")]
    pub description: Option<Option<String>>,
    #[serde(default, with = "::serde_with::rust::double_option")]
    pub deleted_at: Option<Option<DateTime>>,
    #[serde(default, with = "::serde_with::rust::double_option")]
    pub is_default: Option<Option<bool>>,
}
