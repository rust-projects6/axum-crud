use serde::Deserialize;

use crate::util::hashed_pass::HashedPass;

#[derive(Debug, Deserialize)]
pub struct User {
    pub user_name: String,
    pub password: String,
}

impl HashedPass for User {
    fn get_pass(& self) -> &str {
        &self.password
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[tokio::test]
    async fn test_hashing() {
        let user = User {
            user_name: "hiron".to_owned(),
            password: "password".to_owned(),
        };

        let hash = user.get_hashed_pass().unwrap();

        assert_eq!(user.is_verify(&hash), Ok(true));
    }
}
