use serde::Serialize;

#[derive(Debug, Serialize)]
pub struct ResponseTask {
    pub id: i32,
    pub title: String,
    pub description: Option<String>,
    pub priority: Option<String>
}
