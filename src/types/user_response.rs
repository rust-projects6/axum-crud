use serde::Serialize;

#[derive(Debug, Serialize)]
pub struct UserResponse<'a>{
    pub user_name: &'a str,
    pub id: &'a i32,
    pub token: &'a str
}