use serde::Deserialize;
use sea_orm::entity::prelude::DateTime;

#[derive(Debug, Deserialize)]
pub struct RequestTask {
    pub title: String,
    pub priority: Option<String>,
    pub completed_at: Option<DateTime>,
    pub description: Option<String>,
    pub deleted_at: Option<DateTime>,
    pub user_id: Option<i32>,
    pub is_default: Option<bool>,
}
